// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

#[allow(unused_imports)]
use crate::auto::*;

#[doc(alias = "GWeatherMoonLatitude")]
pub type MoonLatitude = f64;
#[doc(alias = "GWeatherMoonPhase")]
pub type MoonPhase = f64;
